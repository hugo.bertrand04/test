*** Settings ***
Library    SeleniumLibrary


*** Variables ***
${GRID_URL}   http://selenium:4444/wd/hub
${BROWSER}   Chrome

*** Keyword ***
Youtube
    Open Browser	 https://www.youtube.com/   ${BROWSER} 	remote_url=${GRID_URL}
    Set Screenshot Directory	 EMBED
    Maximize Browser Window
    Capture Page Screenshot
    [Teardown]  Close All Browsers
