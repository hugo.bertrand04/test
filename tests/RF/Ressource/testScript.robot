*** Settings ***
Library    SeleniumLibrary


*** Variables ***
${GRID_URL}   http://selenium:4444/wd/hub
${BROWSER}   Chrome

*** Keyword ***
Youtube
    Open Browser	 
    Set Screenshot Directory	 EMBED
    Maximize Browser Window
    Capture Page Screenshot
    [Teardown]  Close All Browsers
